# Recipe App API Proxy

NGINX Proxy app for recipe app API

## usage

### Environment variables

 * 'LISTEN PORT' - Port to listen on (default: '8000')
 * 'APP_HOST' - Hostname of the app to forward requests to (default: 'app')
 * 'APP PORT' - Port of the app to forward requests to (default: '9000')